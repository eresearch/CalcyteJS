# CalcyteJS - DEPRECATED --- DO NOT USE IT

THIS CODE IS DEPRECATED - DO NOT USE IT

This  WAS a work-in-progress port of the python-based [Calcyte tool](https://codeine.research.uts.edu.au/eresearch/calcyte).

## Status

This is Alpha code.

## About

Calcyte is a toolkit to implement the [RO-Crate] specification:


1.  Generating HTML from RO-Crate `ro-crate-metadata.jsonld` files.
2.  Managing metadata for collections of content via automatically generated
    spreadsheets (CATALOG.xlsx), to create `ro-crate-metadata.jsonld` files
3.  Packaging data in BagIt format, and optionally zipping it.

This version of Calcyte targets the [R0-Crate Spec, v1.0](https://researchobject.github.io/ro-crate/1.0/).

## Installation

NOTE: These instructions are for MacOs - adapt as necessary.

NOTE: Bagit is no longer required!

- Install [node.js](https://nodejs.org/en/)

- Install Siegfreid using the [instructions](https://github.com/richardlehane/siegfried/wiki/Getting-started).

- Get the code:
 
  git clone --single-branch --branch <branchname> https://code.research.uts.edu.au/eresearch/CalcyteJS.git


- Link the binary for development use:

  npm link




## Usage / instructions




### Usage

Calcyfy creates RO-Crates with an HTML entry-point in ro-crate-preview.html file.

Usage:

```
>  ./calcyfy
   Usage: calcyfy [options] <directories...>

  Generates [RO-Crate] HTML for ro-crate-metadata.jsonld files. Pass a list of directories. To create Spreadsheet files for entering metadata use -d or -r.


  Options:

    -V, --version                    output the version number
    -b,  --bag [bag-dir]             Create Bagit Bag(s) under [bag-dir])
    -n,  --no                        No Citation - only applies ith --bag
    -z,  --zip                       Create a zipped version of the bag - only applies with --bag
    -d,  --depth [depth]             Maximum depth to recurse into directories
    -r,  --recurse                   Recurse into directories looking for CATALOG_.xslx files
    -c,  --cratescript [cratesript]  URL of "crate.js" rendering script from the ro-crate-js library
    -u, --url [distro]               Distribution URL
    -h, --help                       output usage information
```

To run Calcyte on a group of directories pass it a list of directories

To generate an HTML file for an `ro-crate-metadata.jsonld` file in ./dir

```
calcyfy dir
```

One directory:

```

calcyfy -r test_data/sample/ 
calcyfy test_data/Glop_Pot -r -c https://data.research.uts.edu.au/examples/ro-crate/examples/src/crate.js
```

This will:
- Traverse the entire Glop_Pot directory, and generate or update CATALOG_name.xlsx files.
- Create or update the `test_data/Glop_Pot/`ro-crate-metadata.jsonld`` file
- Create a *[RO-Crate] Website* with entry-point `test_data/Glop_Pot/ro-crate-metadata.html`

All the sample directories:

```
calcyfy -r test_data/* -c https://data.research.uts.edu.au/examples/ro-crate/examples/src/crate.js
```

Calcyte will generate:

- a CATALOG\_$dir.xlsx file in each directory (this is for humans to fill in with
  metadata about the data)

- An `ro-crate-preview.html` file summarizing the data using metadata from CATALOG\_$dir.xlsx

- An `ro-crate-metadata.jsonld` file containing JSON-LD metadata derived from the CATALOG\* files plus some basic file-format information.

See the examples in `test_data`.

TODO: Instructions for filling in the CATALOG files.

[RO-Crate]: https://github.com/UTS-eResearch/RO-Crate
