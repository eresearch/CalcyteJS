
/* This is part of Calcyte a tool for implementing the DataCrate data packaging
spec.  Copyright (C) 2018-2019  University of Technology Sydney

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* EXPERIMENTAL HTML Rendering of RO-Crate-metadata.jsonld files */

/* TODO - this borrows code from Calcytes' JSON-LD helper using copy and paste -
need to import that properly using some kind of build system */

const PAGE_SIZE = 37;
var items_by_id = {};
var root = {};


const display_keys = [
    "@id",
    "name",
    "familyName",
    "givenName",
    "@type",
    "description",
    "funder",
    "memberOf",
    "isPartOf",
    "fileOf",
    "thumbnail",
    "datePublished",
    "creator",
    "path",
    "encodingFormat",
    "contentSize",
    "affiliation",
    "email",
    "@reverse"
  ];


  const back_links = {
    hasFile: "fileOf",
    hasPart: "isPartOf",
    hasMember: "memberOf",
    memberOf: "hasMember"
  };
  const back_back_links = new Set(Object.values(back_links))

 function reference_to_item (node) {
    // Check if node is a reference to something else
    // If it is, return the something else
    if (node["@id"] && items_by_id[node["@id"]]) {
        return this.items_by_id[node["@id"]]
    }
    else {
        return null
    }
}

function init() {
  for (let item of meta["@graph"]){
    items_by_id[item["@id"]] = item;
    if (item.path && item.path == "./") {
        root = item;
    }
  }
}

function make_back_links (item) {
  for (let key of Object.keys(item)) {
      if (key != "@id" && key != "@reverse") {
          for (let part of valueAsArray(item[key])) {
              var target = reference_to_item(part);
              var back_link = back_links[key];
              // Dealing with one of the known stuctural properties
              if (target && back_link) {
                  if (!target[back_link]) {
                      //console.log("Making link", key, back_link, target)
                      target[back_link] = [{ "@id": item["@id"] }];
                  }
              } else if (
                  !back_link && target && !back_back_links.has(key)
              ) {
                  // We are linking to something
                  //console.log("Doing a back link", key, target['name'], item['name'])
                  if (!target["@reverse"]) {
                      target["@reverse"] = {};
                  }
                  if (!target["@reverse"][key]) {
                      target["@reverse"][key] = [];
                  }

                  var got_this_reverse_already = false;
                  for (let r of target["@reverse"][key]) {
                    if (r["@id"] === item["@id"]) {
                      got_this_reverse_already = true
                     }
                  }
                  if (!got_this_reverse_already) {
                      //console.log("Back linking", key)
                      target["@reverse"][key].push({ "@id": item["@id"] });
                  }
                  //console.log(JSON.stringify(target, null, 2))
              }
          }
      }
  }
}

function add_back_links () {
  // Add @reverse properties if not there
  for (let item of meta["@graph"]) {
      make_back_links(item);
  }
}




// Work out what order to display metadata in
function sortKeys(keys) {
    // Sort a set or array of keys to be in a nice oeder
    // Returns set
    var keys_in_order = new Set();
    keys = new Set(keys);
    for (let key of display_keys) {
      if (keys.has(key)) {
        keys_in_order.add(key);
      }
    }
    for (let key of keys) {
      if (!keys_in_order.has(key)) {
        keys_in_order.add(key);
      }
    }
    return keys_in_order;
}

// Add labels
function addGloss() {
    $.getJSON( meta["@context"], function( data ) {
    $("th").each(
        function (th) {
            prop = $(this).text();
            href = data['@context'][prop];
            if (href) {
                $(this).append($("<span>&nbsp;</span>")).append($("<a>").html("[?]").attr('href', href));
            }
        }
    )
    }
    )
}

// A single name/value pair of metadata
function addMetadataRow(target, item, prop){
  var row = $("<tr><th style='text-align:left'> </th><td style='text-align:left'> </td></tr>");
  row.find("th").html(prop).attr("style","white-space: nowrap; width: 1%;");
  row.find("td").append(displayValues(item[prop]));
  if (prop === "path") {
      row.find("td span").each(
          function() {
              var path = $(this).text();
              $(this).html($("<a>").attr("href", path ).html(path))
          }
    ) 
  }
  if (row.find("td").text().trim() != "") {
    target.append(row);
  }
}

// Display an item, pass in an id "@id"
function display(id) {
    makeCitation();
    var item = items_by_id[id]
    var name = item.name ? item.name : id;
    $("h3.name").html(name);
    $("#summary").html("");
    var mainTable = $("<table class='table'><tbody/><table>");
    $("#summary").append(mainTable);
    var tbody = mainTable.find("tbody");
    tbody.append($("SOMETHING"))
    for (let prop of sortKeys(Object.keys(item))) {
        if (prop === "@reverse") {
          var row = $("<tr><th style='text-align:left'> </th><td style='text-align:left'> </td></tr>");
          row.find("th").html("Referenced by").attr("style","white-space: nowrap; width: 1%;");
          var t = $("<table class='table'><tbody/><table>");
          var  back = t.find("tbody");
          for (let backProp of Object.keys(item["@reverse"])){
            addMetadataRow(back, item["@reverse"], backProp);
          }
          row.find("td").append(back);
          tbody.append(row);
        }
        else {
          addMetadataRow(tbody, item, prop);
      }
      
    }
    addGloss();
}


function valueAsArray (value) {
            if (!value) {
                return []
            }
            else if (!Array.isArray(value)) {
                return [value];
            } else {
                return value;
            }
        }


function displayValues(v, details) {
    var vals = valueAsArray(v);
    const l = vals.length;
    if (l === 1) {
        // Singleton value - display it
        return(displayValue(vals[0]));
    }
    else if (l <= PAGE_SIZE) {
        var div = $("<div>");
        var list  = $("<ul>");
        var detail_el;
        if (details) {
           var details_el =  $("<details></details>");
           var summary = $("<summary>");
           details_el.append(summary);
           details_el.append(list);
           summary.append(displayValue(vals.slice(0, 1)[0]));
           summary.append($("<span>--to--</span>"))  ;
           summary.append(displayValue(vals.slice(l-1,l)[0]));
        }
        else {
            details_el = list;
        }
        vals.map(
            (v) => {
                var li = $("<li>");
                li.html(displayValue(v));
                list.append(li);
            }   
        )
        return details_el;
    }
    else if (l <= PAGE_SIZE * PAGE_SIZE) {
        var div  = $("<div>");
        div.append(displayValues(vals.slice(0, PAGE_SIZE), true, "first"));
        div.append(displayValues(vals.slice(PAGE_SIZE, l), true, "second"));
        return div;
    }
    else {
        var div  = $("<div>");
        var details_el =  $("<details></details>").append(list);
        div.append(details_el);
        var summary = $("<summary>");
        details_el.append(summary);
        summary.append(displayValue(vals.slice(0,1)[0]));
        summary.append($("<span>--to--</span>"))  ;
        summary.append(displayValue(vals.slice(PAGE_SIZE, l)[0]));
        details_el.append(displayValues(vals.slice(0, PAGE_SIZE * PAGE_SIZE), true));
        div.append(displayValues(vals.slice(PAGE_SIZE * PAGE_SIZE, l), true));
        return div;
    }

}

function displayValue(val) {
    if (val["@id"]) {
        var target = items_by_id[val["@id"]];
        if (target) {
            var name = target.name
            if (!name) {
                name = "-->";
            }
            return $("<a href='#" + target["@id"] + "'> </a>").html(name).attr("onclick", "display('" + target["@id"] + "')" );
        }
    }
    /*
    else if (val.match(/^https?:\/\//i)) {
        return $("<a>").attr("href", val).html(val);
    }
    */
    else {
        return $("<span>").html(val);
    }
}

function makeCitation() {
     var can_cite = true;
      var report = "";
      var creators_strings = [];
      if (root["creator"]) {
        var creators = root["creator"];
        if (!Array.isArray(creators)) {
          creators = [creators];
        }

        for (var i = 0; i < creators.length; i++) {
          var found_creator = false;
          var creator_names = "";
          if (!items_by_id[creators[i]["@id"]]) {
            found_creator = true;
            creators_strings.push(creators[i]);
          } else {
            var creator = items_by_id[creators[i]["@id"]];
            var creator_el;
            //console.log("Looking at creator", creators[i])
            if (creator["familyName"] && creator["givenName"]) {
             
              found_creator = true;
              creators_strings.push(creator_names);
            } else if (creator["name"]) {
              //console.log("Got a name");
            
              creators_strings.push(creator["name"]);
              creator_names = creator["name"];
              found_creator = true;
            }
         
          }
        }
      }

      if (creators_strings.length === 0) {
        can_cite = false;
        report +=
          "Data citations requires *  At least one [schema:creator] with a [schema:givenName] and [schema:familyName]. ";
      }
      if (root["@id"]) {
        var identifier = root["@id"];
        if (identifier.match(/https?:\/\/(dx\.)?doi.org\/10\./)) {
          //<identifier identifierType="DOI">10.5072/example-full</identifier>
       
        } else {
          can_cite = false;
          report += "Collection needs to have a DOI as an ID. ";
        }
      } else {
        report += "There is no Identifier. ";
        can_cite = false;
      }

      var name = "";
      if (root["name"]) {
      
  
        name = root["name"];
      } else {
        can_cite = false;
        report +=
          "Data Citation requires at least one [schema:name] (Title) (which maps to a DataCite title). ";
      }

      if (root["publisher"]) {
        /*
            <publisher>DataCite</publisher>
           */
        var publisher = root["publisher"];
        valueAsArray(publisher, function(pub) {
          publisher = pub[0];
        });
        if (
          publisher["@id"] &&
          items_by_id[publisher["@id"]] &&
          items_by_id[publisher["@id"]].name
        ) {
          publisher = items_by_id[publisher["@id"]].name;
        }
   
      } else {
        can_cite = false;
        report +=
          "At least one schema:publisher property which SHOULD be an organization but may be a String. ";
      }
      var published;
      if (root["datePublished"]) {
        var date =  valueAsArray(root["datePublished"])[0];
          if (date.match(/^\d\d\d\d/)) {
            published = root["datePublished"].slice(0, 4);
          }
        };
      

      if (!published) {
        can_cite = false;
        report += "A schema:datePublished. ";
      }
     
      

      var citation = "";
      if (can_cite) {
        //Datacite text citation:
        //Creator (PublicationYear): Title. Version. Publisher. ResourceType. Identifier
        //console.log(creators_strings);

        citation += creators_strings.join("; ");
        citation += ` (${published}) `;
        citation += `${name}. `;
        citation += `${publisher}. `;
        citation += "Datacrate. ";
        citation += identifier;
      
      } else {
        console.log("Can't cite this dataset:  ", report);
        citation = report;
      }


   
      $("h4.citation").html("Cite as:"  + citation);
}



// Build an index


//add_back_links();