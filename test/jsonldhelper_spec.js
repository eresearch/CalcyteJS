/* This is part of Calcyte a tool for implementing the DataCrate data packaging
spec.  Copyright (C) 2018  University of Technology Sydney

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const fs = require("fs");
const jsonld = require("../lib/jsonldhelper.js");
const path = require("path");
const assert = require("assert");

const fixtures = require("./fixtures");


describe("JSON-LD helper simple tests", function () {

  var test_path;

  before(function () { test_path = fixtures.buildup('context_trimming'); });
  after(function () { fixtures.teardown('context_trimming'); });




  



  it("Test basic indexing", function (done) {
    const json = {
      "@graph": [
        { "@id": "1", "name": "one", "path": "./nothing", "@type": "Test" }
      ]
    }
    var helper = new jsonld()
    helper.init(json)
    assert.equal(
      helper.item_by_id["1"]["name"], "one"
    );
    assert.equal(
      helper.item_by_url["./nothing"]["name"], "one"
    );
    assert.equal(
      helper.item_by_type["Test"][0]["name"], "one"
    );

    assert.equal(
      helper.item_by_type["Test"].length, 1
    );

  
    const json1 = {
      "@graph": [
        {
          "@id": "1", "name": "one", "path": "./nothing", "@type": "Test",
          "hasPart": [{ "@id": "2" }, { "@id": "3" }, { "@id": "4" }], "creator": { "@id": "2" }
        },
        { "@id": "2", "name": "two", "path": "./something", "@type": "Test" },
        { "@id": "3", "name": "three", "path": "./somethin_else", "@type": "Test1" },
        { "@id": "4", "name": "four", "path": "./nothin", "@type": "Test1" }
      ]
    }
   
    helper = new jsonld()
    helper.init(json1)
    assert.equal(
      helper.item_by_id["1"]["name"], "one"
    );
    assert.equal(
      helper.item_by_url["./something"]["name"], "two"
    );
    assert.equal(
      helper.item_by_type["Test"].length, 2
    );
    for (let part of helper.value_as_array(helper.item_by_id["1"]["hasPart"])) {
      assert.equal(helper.reference_to_item(part)["path"].startsWith("./"), true)
    }

    // Check that inverse links have been put in place
    helper.add_back_links()
    assert.equal(helper.item_by_id["2"]["isPartOf"][0]["@id"], "1")
    assert.equal(helper.item_by_id["3"]["isPartOf"][0]["@id"], "1")
    assert.equal(helper.item_by_id["4"]["isPartOf"][0]["@id"], "1")
    assert.equal(helper.item_by_id["2"]["@reverse"]["creator"][0]["@id"], "1")
    
    
    


    
   

    done();
  });
});
